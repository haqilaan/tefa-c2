import React from 'react';
import { useParams } from 'react-router-dom';


export default function ProductsDetail() {
  const params = useParams();
  console.log(params);
  return (
    <main>
      <h1 className="text-red-900 flex justify-center my-20 px-12 py-2">PRODUCTS DETAIL{params.id}</h1>
    </main>
  );
}
  