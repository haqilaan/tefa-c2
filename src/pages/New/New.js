import React, { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import './new.css';
import BtnSave from '../../components/elements/BtnSave/BtnSave';
import Select from '../../components/elements/Select/Select';
import InputFIeld from '../../components/elements/InputFIeld/InputFIeld';
import TextArea from '../../components/elements/TextArea';

export default function New() {
  const location = useLocation();
  const currentLocation = location.pathname;
  const navigate = useNavigate();

  const [ name, setName ] = useState('');
  const [ price, setPrice ] = useState(0);
  const [ category, setCategory ] = useState('');
  const [ description, setDescription ] = useState('');
  const [ hasExpiry, setHasExpiry ] = useState(false);
  const [ expiryDate, setDateExpiry ] = useState(null);
  const [image, setProfile] = useState('');

  const onChangeName = (e) => {
    setName(e.target.value);
  };

  const onChangePrice = (e) => {
    setPrice(e.target.value);
  };

  const onChangeCategory = (e) => {
    setCategory(e.target.value);
  };

  const onChangeDescription = (e) => {
    setDescription(e.target.value);
  };

  const onChangeHasExpiry = () => {
    setHasExpiry(!hasExpiry);
  };

  const onChangeExpiryDate = (e) => {
    setDateExpiry(e.target.value);
  };


  const onChangeProfile = async (event) => {
    const file = event.target.files[0];
    const base64 = await convertBase64(file);
    setProfile(base64);
  };
  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resolve(fileReader.result);
      };
      fileReader.onerror = (error) => {
        reject(error);
      };

    });
  };

  const onSubmitActive = ()=>{
    const newItem = {
      id: 'abj'+Math.floor(Math.random() * 1000),
      name,
      price,
      image,
      description,
      isDeleted: false,
      category,
      expiryDate,
    };

    navigate('../products', { state: newItem });
  };

  const items = (
    <>
      <div className="input-container">
        <InputFIeld
          containerStyle="input"
          inputStyle="input__text"
          label="Product Name"
          labelStyle="input__label"
          name="product-name"
          onChangeInput={onChangeName}
          placeholder="Enter Product Name"
          type="text"
          value={name}
        />
        <InputFIeld
          containerStyle="input"
          inputStyle="input__text"
          label="Product Price"
          labelStyle="input__label"
          name="product-price"
          onChangeInput={onChangePrice}
          placeholder="Enter Price"
          type="text"
          value={price}
        />
        <Select
          containerStyle="select"
          defaultValue="Chose Category"
          label="Product Category"
          labelStyle="select__label"
          name="select-items"
          onChangeSelect={onChangeCategory}
          selectStyle="select__items"
          value={category} 
        />
      </div>
      <div className="split">
        <TextArea
          containerStyle="textarea"
          label="Description"
          labelStyle="textarea__label"
          name="description"
          onChangeDesc={onChangeDescription}
          placeholder="Enter Product Description"
          textAreatStyle="textarea__text"
          value={description}
        />
        <div className="input__fileUpload">
          <div className="image-preview">
            <img className="image" src={image}/>
          </div>
          <label className="input-Tag"> Upload Image
            <input
              id="inputTag"
              onChange={onChangeProfile}
              type="file"
            />
          </label>
          </div>
        </div>
        <InputFIeld
          containerStyle="checkbox"
          inputStyle="checkbox__text"
          label="Product has expiry date"
          labelStyle="checkbox__label"
          onChangeInput={onChangeHasExpiry}
          type="checkbox"
          value={hasExpiry}
        />
        <InputFIeld
          containerStyle="input"
          inputStyle="input__date"
          label="Date Expiry"
          labelStyle="input__label"
          name="date-expiry"
          onChangeInput={onChangeExpiryDate}
          type="date"
          value={expiryDate}
          />
    </>  
  );

  return (
    <main className="new-product">
      <div className="path">
        {currentLocation.split('/').map((maping) => maping + ' / ')}
      </div>
      <hr className="devider"/>
        <BtnSave items={items} onSubmitActive = {onSubmitActive}></BtnSave>
    </main>
    
  );
}
