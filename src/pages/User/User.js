import React from 'react';
import user from './user.json';
import Table from '../../components/elements/Table';
import BtnDel from '../../components/elements/BtnDel/BtnDel';
//import Layouts from '../../components/elements/Layouts/Layouts';

export default function Users() {
  const column = ['Nama', 'Email', 'ACTION'];

  const rowsData = (i, idx) => {
    return (
      <tr className="p-2" key={idx}>
        <td className="p-2">{i.username}</td>
        <td className="p-2">{i.email}</td>
        <td className="p-2" ><BtnDel name={i.username}/></td>
      </tr>
    );
  };

  return (
      <main>
        <div className="w-[69rem] 2xl:w-[98rem] w-full my-20 mx-72 p-8 shadow-lg bg-white">
          <h1 className="text-[#1E293B] p-12 text-2xl font-bold">User</h1>
          <div className="border-b" />
          <div className="px-5 py-5 ">
              <Table columns={column} items={user} rows={rowsData} />
          </div>
        </div>
      </main>
  );
}
