import React, { lazy, Suspense } from 'react';
import LazyFallback from '../components/elements/LazyFallback';

const Suspensed = (Element) => function suspense(props) {
  return (
    <Suspense fallback={<LazyFallback />}>
      <Element {...props} />
    </Suspense>
  );
};

export const Main = Suspensed(lazy(() => import('./Main')));
export const Products = Suspensed(lazy(() => import('./Products')));
export const New = Suspensed(lazy(() => import('./New')));
export const ProductsDetail = Suspensed(lazy(() => import('./ProductsDetail')));
export const User = Suspensed(lazy(() => import('./User')));
