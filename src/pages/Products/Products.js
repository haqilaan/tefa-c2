import React, { useState, useEffect } from 'react';
import data from './product.json';
import Table from '../../components/elements/Table';
import {Link, useLocation} from 'react-router-dom';
import BtnDel from '../../components/elements/BtnDel/BtnDel';
import BtnAdd from '../../components/elements/BtnAdd/BtnAdd';
import InputFIeld from '../../components/elements/InputFIeld/InputFIeld';
import Select from '../../components/elements/Select/Select';
import dateFormat from 'dateformat';

//import Layout from '../../components/elements/Layouts/Layouts';
//import Header from '../../components/elements/Header';

export default function Products() {
  const [itemsData, setItemsData] = useState(data);
  const [category, setCategory] = useState("");
  const [check, setCheck] = useState(false);
  
  const { state } = useLocation();
  useEffect(()=>{
    console.log(state);
    setItemsData(state!== null? [ state, ...itemsData ] : [...itemsData]);
    // setItemsData([ {}, ...itemsData ]);
  },[state]);
  console.log(itemsData);
  
  useEffect(()=>{
    if(state !== null){
      const filterData = [state, ...data].filter(i => {
        const hasExpired = i.expiryDate < new Date().toISOString().slice(0, 10);
        if(check && !category ){
          return !hasExpired || !i.expiryDate;
        }else if(check && category){
          return (!hasExpired || !i.expiryDate) && i.category === category;
        }else if(!check && !category){
          return true;
        }else{
          return i.category === category;
        }
      });
      setItemsData(filterData);
    } else{
      const filterData = data.filter(i => {
        const hasExpired = i.expiryDate < new Date().toISOString().slice(0, 10);
        if(check && !category ){
          return !hasExpired || !i.expiryDate;
        }else if(check && category){
          return (!hasExpired || !i.expiryDate) && i.category === category;
        }else if(!check && !category){
          return true;
        }else{
          return i.category === category;
        }
      });
      setItemsData(filterData);
    }
    console.log('useEffect called 2');
  },[check, category]);
    
  

  
  const onChangeSelect = (e) => {
    setCategory(e.target.value);
  };

  const onChangeHideActive = () => {
    setCheck(!check);
  };

  const onDelete = (id) => {
    const newData = data.map((i) => {
      if (i.id === id) {
        i.isDeleted = true;
        console.log(i.isDeleted);
      }
      return i;

    });
    setItemsData(newData);
    
  };
  

  const column = ['Product Name', 'Description', 'Product Price', 'Category', 'Expiry Date', 'ACTION'];
    const rowsData = (i, idx) => {
      return (
          <>
            {
              !i.isDeleted && 
              <tr className="p-2" key={idx}>
                <td className={`p-2 ${i.expiryDate < new Date().toISOString().slice(0, 10)? 'opacity-50':''}`}>
                  <Link to={`/products/${i.id}`}>
                      <img className="rounded-full mr-3" src={i.image} width={70} />{i.name}
                  </Link>
                </td>
                <td className={`p-2 ${i.expiryDate < new Date().toISOString().slice(0, 10)? 'opacity-50':''}`}>{i.description}</td>
                <td className={`p-2 text-green-400 ${i.expiryDate < new Date().toISOString().slice(0, 10)? 'opacity-40':''}`}>Rp{numberWithDot(i.price)}</td>
                <td className={`p-2 ${i.expiryDate < new Date().toISOString().slice(0, 10)? 'opacity-50':''}`}>{i.category}</td>
                <td className={`p-2 ${i.expiryDate < new Date().toISOString().slice(0, 10)? 'opacity-50':''}`}>{dateFormat(i.expiryDate, 'd mmmm yyyy')}</td>
                <td className="p-2" >
                  <BtnDel onDelete={()=>onDelete(i.id)} name={i.name}/>
                </td>
              </tr>
            }
          </>
        
      );
    };
  
    //Membuat titik ribuan pada price
    const numberWithDot = (number) => {
      return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
    };
  
    return (
      <main>
        <div className="w-[69rem] 2xl:w-[98rem] my-20 mx-72 p-8 shadow-lg bg-white overflow-y-auto">
          <div className="p-12 flex justify-between">
            <h1 className="mb-4 text-2xl font-bold text-[#1E293B]">Products</h1>
            <div className="flex">
              <InputFIeld onChangeInput={onChangeHideActive} value={check} containerStyle="checkbox" inputStyle="checkbox__text" label="Hide expired product" labelStyle="checkbox__label" type="checkbox"/>
              <Select onChangeSelect={onChangeSelect} value={category} containerStyle="select" defaultValue="Chose Category" labelStyle="select__label" selectStyle="select__addnew"/>
              <BtnAdd dest="/products/new" style="btn btn__normal btn--addnew" type={null}>Add New Product</BtnAdd>
            </div>
          </div>
          <div className="border-b" />
          <div className="px-5 py-5">
            <Table columns={column} items={itemsData} rows={rowsData} />
          </div>
        </div>
      </main>

    );
  }
  