import React from 'react';
import './select.css';
import PropTypes from 'prop-types';


export default function Select({ containerStyle, label, labelStyle, selectStyle, onChangeSelect, value }) {
  
  return (
    <div className={containerStyle}>
      <label className={labelStyle}>{label}</label>
      <select onChange={onChangeSelect} value={value} className={selectStyle} >
        <option value="">All Category</option>
        <option value="Ready">Ready</option>
        <option value="Barang Bekas">Barang Bekas</option>
        <option value="Pre-Order">Pre-Order</option>
      </select>
    </div>
  );
}

Select.propTypes = {
  onChangeSelect: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  containerStyle: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  selectStyle: PropTypes.string.isRequired
};

