import React from 'react';
import nav1 from '../../../assets/nav1.png';
import nav2 from '../../../assets/nav2.png';
import nav3 from '../../../assets/nav3.png';
import nav4 from '../../../assets/nav4.png';



export default function Header(){
  return (
    <nav className="flex w-full fixed justify-end top-0 h-14 py-2 bg-white text-[#475569] font-bold mx-auto">
      <div className="flex float-left bg-gray-100 rounded-full p-3 mx-2">
        <img src={nav4}/>
      </div>
      <div className="flex float-left bg-gray-100 rounded-full p-3 mx-2">
        <img src={nav3}/>
      </div>
      <div className="flex float-left bg-gray-100 rounded-full p-3 mx-2">
        <img src={nav2}/>
      </div>
      <div className="flex float-left pr-2">
        <img src={nav1}/>
        <p className="px-2 mx-2 font-bold text-lg py-1">Acne</p>
      </div>
    </nav>
  );
}
