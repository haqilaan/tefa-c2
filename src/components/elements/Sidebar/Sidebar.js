import React from 'react';
import logo from '../../../assets/logo.png';
import product from '../../../assets/product.png';
import user from '../../../assets/user.png';
import {Link} from 'react-router-dom';




export default function Sidebar(){
  return (
    <div className="top-0 left-0 bg-[#1E293B]  p-2 pl-2 text-white fixed h-full w-64">
      <ul className="px-2">
        <li className="pt-10 pb-8">
          <img src={logo}/>
        </li>
        <li className="text-[#64748B] pb-8">
          PAGES
        </li>
        <Link to="/products">
          <li className="pb-8">
            <img className="float-left pr-5 " src={product}/>Product
          </li> 
        </Link>
        <Link to="/user">
          <li className="pb-8">
            <img className="float-left pr-5" src={user}/>User
          </li>  
        </Link>
      </ul>
    </div>
    
  );
}
