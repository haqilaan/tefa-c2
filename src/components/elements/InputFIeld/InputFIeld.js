import React from 'react';
import './InputField.css';
import PropTypes from 'prop-types';

export default function InputFIeld({ onChangeInput, value, containerStyle, inputStyle, isDisable, label, labelStyle, placeholder, type }) {
  return (
    <div className={containerStyle}>
      <label className={labelStyle}>{label}</label>
      <input onChange={onChangeInput} value={value} className={inputStyle}  disabled={isDisable} placeholder={placeholder} type={type}/>
    </div>
  );
}

InputFIeld.propTypes = {
  onChangeInput: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  containerStyle: PropTypes.string.isRequired,
  inputStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
};

