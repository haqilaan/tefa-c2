import React from 'react';
import PropTypes from 'prop-types';

export default function Table({ items, columns, rows }) {
    return (
      <table className="w-full text-gray-500 p-8 mb-6">
        <thead className="border-b bg-[#F8FAFC]">
          <tr>
            {columns.map((i, idx) => (
              <th className="p-2 text-slate-600" key={idx}>
                {i}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>{items.map(rows)}</tbody>
      </table>
    );
  }
  Table.propTypes = {
    columns: PropTypes.array.isRequired,
    items: PropTypes.array.isRequired,
    rows: PropTypes.func.isRequired,
  };
  
  