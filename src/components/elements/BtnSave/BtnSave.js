import React from 'react';
import BtnAdd from '../BtnAdd';
import './btnsave.css';
import PropTypes from 'prop-types';

export default function BtnSave({onSubmitActive, items }) {
  return (
    <form onSubmit={onSubmitActive}>
      {items}
      <div className="form__action">
        <BtnAdd dest="/products" style="btn--secondary">Cancel</BtnAdd>
        <BtnAdd dest={null} style="btn--primary" type="submit">Save</BtnAdd>
      </div>
    </form>
  );
}

BtnSave.propTypes = {
  items: PropTypes.array.isRequired,
  onSubmitActive: PropTypes.func.isRequired,
};
