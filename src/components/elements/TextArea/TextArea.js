import React from 'react';
import PropTypes from 'prop-types';
import './TextArea.css';


export default function TextArea({ containerStyle, label, labelStyle, name, onChangeDesc, placeholder, textAreatStyle }) {
  return (
    <div className={containerStyle}>
      <label className={labelStyle} htmlFor={name}>{label}</label>
      <textarea onChange={onChangeDesc} className={textAreatStyle} name={name} placeholder={placeholder}/>
    </div>
  );
}

TextArea.propTypes = {
  containerStyle: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChangeDesc: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  textAreatStyleStyle: PropTypes.string.isRequired,
};
