import React from 'react';
import Sidebar from '../Sidebar/Sidebar';
import Header from '../Header/Header';
import PropTypes from 'prop-types';


export default function Layout(){
  return (
    <>
      <Header/>
      <Sidebar/>
    </>
  );
}
