import React from 'react';
import PropTypes from 'prop-types';
import { hot } from 'react-hot-loader/root';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import ErrorBoundary from './components/elements/ErrorBoundary';
import { Main, Products, ProductsDetail, New, User } from './pages';
import Layout from './components/elements/Layout';

const App = ({ store }) => {
  return (
    <ErrorBoundary>
      <Provider store={store}>
        <BrowserRouter>
        <Layout />
          <Routes>
            <Route element={<Main />} exact path="/" />
            <Route element={<Products />} exact path="/products" />
            <Route element={<New />} exact path="/products/new" />
            <Route element={<ProductsDetail />} exact path="/products/:id" />
            <Route element={<User />} exact path="/user" />
          </Routes>
        </BrowserRouter>
      </Provider>
    </ErrorBoundary>
  );
};

export default hot(App);

App.propTypes = {
  store: PropTypes.object.isRequired,
};
